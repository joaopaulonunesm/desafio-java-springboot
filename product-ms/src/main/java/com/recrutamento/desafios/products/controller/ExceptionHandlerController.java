package com.recrutamento.desafios.products.controller;

import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.recrutamento.desafios.products.dto.ErrorResponseDTO;
import com.recrutamento.desafios.products.enumerator.Error;
import com.recrutamento.desafios.products.exception.ServiceException;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@ControllerAdvice
public class ExceptionHandlerController {

	private MessageSource messageSource;
	
	public ExceptionHandlerController(MessageSource messageSource) {
		this.messageSource = messageSource;
	}

	@ExceptionHandler(ServiceException.class)
	public ResponseEntity<ErrorResponseDTO> serviceExceptionHandler(ServiceException ex){
		
		String messageError = messageSource.getMessage(ex.getCodeError(), ex.getParameters(), LocaleContextHolder.getLocale());
		
		HttpStatus httpStatus = HttpStatus.BAD_REQUEST;

		if(Error.PR_1.getCode().equals(ex.getCodeError())) {
			httpStatus = HttpStatus.NOT_FOUND;
		}
		
		return ResponseEntity.status(httpStatus)
							 .body(new ErrorResponseDTO(httpStatus.value(), messageError));
	}
	
	@ExceptionHandler(MethodArgumentNotValidException.class)
	public ResponseEntity<ErrorResponseDTO> handleMethodArgumentNotValidExceptionException(MethodArgumentNotValidException exception) {
		
		String message = "";
		
		BindingResult bindingResult = exception.getBindingResult();
		
		for (FieldError fieldError : bindingResult.getFieldErrors()) {
			String campo = fieldError.getField();
			String mensagem = fieldError.getDefaultMessage();
			message += "Campo " + campo +  ": " + mensagem + ". "; 
		}
		
		return ResponseEntity.status(HttpStatus.BAD_REQUEST)
				 			 .body(new ErrorResponseDTO(HttpStatus.BAD_REQUEST.value(), message.trim()));
	}
	
	@ExceptionHandler(Exception.class)
	public ResponseEntity<ErrorResponseDTO> dafultExceptionHandler(Exception ex){
				
		log.error("Erro no processamento da requisição: {}", ex.getLocalizedMessage());
		
		String messageError = messageSource.getMessage(Error.PR_99.getCode(), null, LocaleContextHolder.getLocale());
		
		return ResponseEntity.status(HttpStatus.BAD_REQUEST)
							 .body(new ErrorResponseDTO(HttpStatus.BAD_REQUEST.value(), messageError));
	}
	
}
