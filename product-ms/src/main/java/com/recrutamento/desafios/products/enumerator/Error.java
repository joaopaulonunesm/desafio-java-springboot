package com.recrutamento.desafios.products.enumerator;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum Error {

	PR_1("PR-1"), PR_99("PR-99");
	
	private String code;
}
