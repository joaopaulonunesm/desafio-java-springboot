package com.recrutamento.desafios.products.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class ErrorResponseDTO {

	@JsonProperty("status_code")
	private Integer statusCode;
	
	@JsonProperty("message")
	private String message;
	
}
