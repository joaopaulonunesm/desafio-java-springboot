package com.recrutamento.desafios.products.exception;

import java.util.Arrays;

import com.recrutamento.desafios.products.enumerator.Error;

import lombok.Getter;

@Getter
public class ServiceException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	private String codeError;
	private Object[] parameters;
	
	public ServiceException(Error error, Object... parameters) {
		super(error.getCode() + " - " + Arrays.toString(parameters));
		this.codeError = error.getCode();
		this.parameters = parameters;
	}

}
