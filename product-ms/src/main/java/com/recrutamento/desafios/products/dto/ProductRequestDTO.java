package com.recrutamento.desafios.products.dto;

import java.math.BigDecimal;

import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class ProductRequestDTO {

	@NotNull
	private String name;
	
	@NotNull
	private String description;

	@NotNull
	@Digits(integer = 20, fraction = 2)
	@Positive
	private BigDecimal price;
}
