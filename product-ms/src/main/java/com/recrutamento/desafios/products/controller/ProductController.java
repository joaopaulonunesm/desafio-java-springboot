package com.recrutamento.desafios.products.controller;


import java.math.BigDecimal;
import java.util.List;

import javax.validation.Valid;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.recrutamento.desafios.products.dto.ProductRequestDTO;
import com.recrutamento.desafios.products.dto.ProductResponseDTO;
import com.recrutamento.desafios.products.service.ProductService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/products")
public class ProductController {

	private ProductService productService;

	public ProductController(ProductService productService) {
		this.productService = productService;
	}
	
	@PostMapping
	public ResponseEntity<ProductResponseDTO> insert(@Valid @RequestBody ProductRequestDTO product){
		log.info("Iniciando POST em /products com request {}", product);
		
		ProductResponseDTO productResponseDTO = productService.insert(product);
		
		log.info("Finalizando POST em /products com response {}", productResponseDTO);
		
		return ResponseEntity.status(HttpStatus.CREATED).body(productResponseDTO);
	}
	
	@PutMapping("/{id}")
	public ResponseEntity<ProductResponseDTO> alter(@PathVariable Long id, @Valid @RequestBody ProductRequestDTO product){
		log.info("Iniciando PUT em /products/{id} com id {} e com request {}", id, product);
		
		ProductResponseDTO productResponseDTO = productService.alter(id, product);
		
		log.info("Finalizando PUT em /products/{id} com id {} e com response {}", id, productResponseDTO);
		
		return ResponseEntity.ok(productResponseDTO);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<ProductResponseDTO> findById(@PathVariable Long id){
		log.info("Iniciando GET em /products/{id} com id {}", id);
		
		ProductResponseDTO productResponseDTO = productService.findByIdDTO(id);
		
		log.info("Finalizando GET em /products/{id} com id {}", id);
		
		return ResponseEntity.ok(productResponseDTO);
	}
	
	@GetMapping
	public ResponseEntity<List<ProductResponseDTO>> findAll(){
		log.info("Iniciando GET em /products");
		
		List<ProductResponseDTO> products = productService.findAll();
		
		log.info("Finalizando GET em /products");
		
		return ResponseEntity.ok(products);
	}
	
	@GetMapping("/search")
	public ResponseEntity<List<ProductResponseDTO>> findByFilters(@RequestParam(value = "q", required = false) String q,
																  @RequestParam(value = "min_price", required = false) BigDecimal minPrice,
			            										  @RequestParam(value = "max_price", required = false) BigDecimal maxPrice){
		log.info("Iniciando GET em /products/search com parametros Q: {}, MinPrice: {}, MaxPrice: {}", q, minPrice, maxPrice);
		
		List<ProductResponseDTO> products = productService.findByFilters(q, minPrice, maxPrice);
		
		log.info("Finalizando GET em /products/search com parametros Q: {}, MinPrice: {}, MaxPrice: {}", q, minPrice, maxPrice);
		
		return ResponseEntity.ok(products);
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<Void> delete(@PathVariable Long id){
		log.info("Iniciando DELETE em /products/{id} com id {}", id);
		
		productService.delete(id);
		
		log.info("Finalizando DELETE em /products/{id} com id {}", id);
		
		return ResponseEntity.status(HttpStatus.OK).build();
	}
}
