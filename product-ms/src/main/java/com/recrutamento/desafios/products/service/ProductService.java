package com.recrutamento.desafios.products.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.stereotype.Service;

import com.recrutamento.desafios.products.dto.ProductRequestDTO;
import com.recrutamento.desafios.products.dto.ProductResponseDTO;
import com.recrutamento.desafios.products.entity.Product;
import com.recrutamento.desafios.products.enumerator.Error;
import com.recrutamento.desafios.products.exception.ServiceException;
import com.recrutamento.desafios.products.repository.ProductRepository;

@Service
public class ProductService {

	private ProductRepository productRepository;

	public ProductService(ProductRepository productRepository) {
		this.productRepository = productRepository;
	}

	@Transactional
	public ProductResponseDTO insert(ProductRequestDTO product) {
		return mapperProductToProductResponseDTO(productRepository.save(mapperProductRequestDTOToProduct(product)));
	}
	
	@Transactional
	public ProductResponseDTO alter(Long id, ProductRequestDTO productChanged) {
		Product product = findById(id);
		
		product.setName(productChanged.getName());
		product.setDescription(productChanged.getDescription());
		product.setPrice(productChanged.getPrice());
		
		return mapperProductToProductResponseDTO(productRepository.save(product));
	}

	@Transactional
	public void delete(Long id) {
		productRepository.delete(findById(id));
	}
	
	public ProductResponseDTO findByIdDTO(Long id) {
		return mapperProductToProductResponseDTO(findById(id));
	}
	
	public List<ProductResponseDTO> findAll(){
		
		List<Product> allProducts = productRepository.findAll();
		
		return mapperProductListToResponseList(allProducts);
	}
	
	public List<ProductResponseDTO> findByFilters(String q, BigDecimal minPrice, BigDecimal maxPrice) {
		
		List<Product> allProducts = productRepository.findByFilters(q != null ? q.toUpperCase() : null, minPrice, maxPrice);
		
		return mapperProductListToResponseList(allProducts);
	}
	
	private Product findById(Long id) {
		return productRepository.findById(id).orElseThrow(() -> new ServiceException(Error.PR_1, id));
	}
	
	private ProductResponseDTO mapperProductToProductResponseDTO(Product product) {
		return new ProductResponseDTO(product.getId(), product.getName(), product.getDescription(), product.getPrice());
	}
	
	private Product mapperProductRequestDTOToProduct(ProductRequestDTO productRequestDTO) {
		return new Product(productRequestDTO.getName(), productRequestDTO.getDescription(), productRequestDTO.getPrice());
	}
	
	private List<ProductResponseDTO> mapperProductListToResponseList(List<Product> products){
		List<ProductResponseDTO> responseProducts = new ArrayList<>();
		products.forEach(p -> responseProducts.add(mapperProductToProductResponseDTO(p)));
		return responseProducts;
	}
}
