package com.recrutamento.desafios.products.repository;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.recrutamento.desafios.products.entity.Product;

public interface ProductRepository extends JpaRepository<Product, Long> {

	@Query("select p 																								   " +
		   "from Product p 																							   " +
		   "where (:q is null or (:q is not null and (upper(p.name) LIKE %:q% or upper(p.description) LIKE %:q%))) and " + 
		   "((:minPrice is not null and p.price >= :minPrice) or :minPrice is null) and 							   " +
		   "((:maxPrice is not null and p.price <= :maxPrice) or :maxPrice is null)")
	List<Product> findByFilters(@Param(value = "q") String q, @Param(value = "minPrice") BigDecimal minPrice, @Param(value = "maxPrice") BigDecimal maxPrice);
	
}
