package com.recrutamento.desafios.products.dto;

import java.math.BigDecimal;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class ProductResponseDTO {

	private Long id;
	private String name;
	private String description;
	private BigDecimal price;
	
}
