package com.recrutamento.desafios.products.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.recrutamento.desafios.products.dto.ProductRequestDTO;
import com.recrutamento.desafios.products.dto.ProductResponseDTO;
import com.recrutamento.desafios.products.entity.Product;
import com.recrutamento.desafios.products.enumerator.Error;
import com.recrutamento.desafios.products.exception.ServiceException;
import com.recrutamento.desafios.products.repository.ProductRepository;

@ExtendWith(MockitoExtension.class)
public class ProductServiceTest {

	@Mock
	private ProductRepository productRepository;
	
	@InjectMocks
	private ProductService productService;
	
	@Test
	public void insertProduct() {
		
		ProductRequestDTO productRequestDTOMock = new ProductRequestDTO("Nome", "Descrição", BigDecimal.TEN);
		ProductResponseDTO productResponseDTOMock = ProductResponseDTO.builder().id(1L)
																				.name(productRequestDTOMock.getName())
																				.description(productRequestDTOMock.getDescription())
																				.price(productRequestDTOMock.getPrice())
																				.build();
		
		when(productRepository.save(mapperProductRequestDTOToProduct(productRequestDTOMock))).thenReturn(mapperProductResponseDTOToProduct(productResponseDTOMock));
		
		ProductResponseDTO productResponseDTO = productService.insert(productRequestDTOMock);
		
		verify(productRepository).save(any(Product.class));
		verifyNoMoreInteractions(productRepository);
        
        assertEquals(productResponseDTOMock, productResponseDTO);
	}
	
	@Test
	public void alterProductWithSuccess() {
		
		ProductRequestDTO productRequestDTOMock = new ProductRequestDTO("Nome", "Descrição", BigDecimal.TEN);
		Product product = new Product(1L, productRequestDTOMock.getName(), productRequestDTOMock.getDescription(), productRequestDTOMock.getPrice());
		ProductResponseDTO productResponseDTOMock = mapperProductToProductResponseDTO(product);
		
		when(productRepository.findById(product.getId())).thenReturn(Optional.of(product));
		when(productRepository.save(product)).thenReturn(product);
		
		ProductResponseDTO productResponseDTO = productService.alter(product.getId(), productRequestDTOMock);
		
		verify(productRepository).findById(any(Long.class));
		verify(productRepository).save(any(Product.class));
		verifyNoMoreInteractions(productRepository);
        
        assertEquals(productResponseDTOMock, productResponseDTO);
	}
	
	@Test
	public void alterProductWithError() {
		
		ProductRequestDTO productRequestDTOMock = new ProductRequestDTO("Nome", "Descrição", BigDecimal.TEN);
		Product product = new Product(1L, productRequestDTOMock.getName(), productRequestDTOMock.getDescription(), productRequestDTOMock.getPrice());
		
		when(productRepository.findById(product.getId())).thenReturn(Optional.empty());
		
		 ServiceException serviceException = assertThrows(ServiceException.class, () -> productService.alter(product.getId(), productRequestDTOMock));
		
		verify(productRepository).findById(any(Long.class));
		verifyNoMoreInteractions(productRepository);
        
        assertEquals(Error.PR_1.getCode(), serviceException.getCodeError());
	}
	
	@Test
	public void deleteProductWithSuccess() {
		
		Product product = new Product(1L, "Nome", "Descrição", BigDecimal.TEN);
		
		when(productRepository.findById(product.getId())).thenReturn(Optional.of(product));
		
		productService.delete(product.getId());
		
		verify(productRepository).findById(any(Long.class));
		verify(productRepository).delete(any(Product.class));
		verifyNoMoreInteractions(productRepository);
	}
	
	@Test
	public void deleteProductWithError() {
		
		Product product = new Product(1L, "Nome", "Descrição", BigDecimal.TEN);
		
		when(productRepository.findById(product.getId())).thenReturn(Optional.empty());
		
		ServiceException serviceException = assertThrows(ServiceException.class, () -> productService.delete(product.getId()));
		
		verify(productRepository).findById(any(Long.class));
		verifyNoMoreInteractions(productRepository);
        
        assertEquals(Error.PR_1.getCode(), serviceException.getCodeError());
	}
	
	@Test
	public void findByIdWithSuccess() {
		
		ProductRequestDTO productRequestDTOMock = new ProductRequestDTO("Nome", "Descrição", BigDecimal.TEN);
		Product product = new Product(1L, productRequestDTOMock.getName(), productRequestDTOMock.getDescription(), productRequestDTOMock.getPrice());
		ProductResponseDTO productResponseDTOMock = mapperProductToProductResponseDTO(product);
		
		when(productRepository.findById(product.getId())).thenReturn(Optional.of(product));
		
		ProductResponseDTO productResponseDTO = productService.findByIdDTO(product.getId());
		
		verify(productRepository).findById(any(Long.class));
		verifyNoMoreInteractions(productRepository);
        
        assertEquals(productResponseDTOMock, productResponseDTO);
	}
	
	@Test
	public void findByIdWithError() {
		
		Product product = new Product(1L, "Nome", "Descrição", BigDecimal.TEN);
		
		when(productRepository.findById(product.getId())).thenReturn(Optional.empty());
		
		ServiceException serviceException = assertThrows(ServiceException.class, () -> productService.findByIdDTO(product.getId()));
		
		verify(productRepository).findById(any(Long.class));
		verifyNoMoreInteractions(productRepository);
        
        assertEquals(Error.PR_1.getCode(), serviceException.getCodeError());
	}
	
	private Product mapperProductResponseDTOToProduct(ProductResponseDTO product) {
		return new Product(product.getId(), product.getName(), product.getDescription(), product.getPrice());
	}
	
	private ProductResponseDTO mapperProductToProductResponseDTO(Product product) {
		return new ProductResponseDTO(product.getId(), product.getName(), product.getDescription(), product.getPrice());
	}
	
	private Product mapperProductRequestDTOToProduct(ProductRequestDTO productRequestDTO) {
		return new Product(productRequestDTO.getName(), productRequestDTO.getDescription(), productRequestDTO.getPrice());
	}
	
}
