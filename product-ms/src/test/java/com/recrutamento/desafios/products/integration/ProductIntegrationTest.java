package com.recrutamento.desafios.products.integration;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.hasSize;

import java.math.BigDecimal;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.ActiveProfiles;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.recrutamento.desafios.products.dto.ProductRequestDTO;
import com.recrutamento.desafios.products.entity.Product;
import com.recrutamento.desafios.products.enumerator.Error;
import com.recrutamento.desafios.products.repository.ProductRepository;

import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.response.ValidatableResponse;
import io.restassured.specification.RequestSpecification;

@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
public class ProductIntegrationTest {

	private static final String URL_BASE = "products";
	
	@Autowired
	private ProductRepository productRepository;
	
	@LocalServerPort
    private int port;

    @BeforeEach
    public void before() {
    	RestAssured.port = port;
    	productRepository.deleteAll();
    }
	
	@Test
	public void insertProductWithSuccess() {

		ProductRequestDTO productRequestDTOMock = new ProductRequestDTO("Nome", "Descrição", BigDecimal.TEN);
		
		createPost(URL_BASE, productRequestDTOMock, HttpStatus.CREATED)
			.body("id", greaterThan(0))
			.body("name", equalTo(productRequestDTOMock.getName()))
			.body("description", equalTo(productRequestDTOMock.getDescription()))
			.body("price", equalTo(productRequestDTOMock.getPrice().intValue()));
	}

	@Test
	public void insertProductWithError() {
		
		ProductRequestDTO productRequestDTOMock = new ProductRequestDTO("Nome", "Descrição", new BigDecimal(-10));
		
		createPost(URL_BASE, productRequestDTOMock, HttpStatus.BAD_REQUEST)
			.body("status_code", equalTo(HttpStatus.BAD_REQUEST.value()))
			.body("message", containsString("price"));
	}

	@Test
	public void alterProductWithSuccess() throws JsonMappingException, JsonProcessingException {

		ProductRequestDTO productRequestDTOMock = new ProductRequestDTO("Nome", "Descrição", BigDecimal.TEN);
		
		Product product = createPost(URL_BASE, productRequestDTOMock, HttpStatus.CREATED).extract().body().as(Product.class);
		
		String nomeAlterado = "Nome Alterado";

		productRequestDTOMock.setName(nomeAlterado);
		
		createPut(URL_BASE + "/" + product.getId(), productRequestDTOMock, HttpStatus.OK)
			.body("id", greaterThan(0))
			.body("name", equalTo(nomeAlterado))
			.body("description", equalTo(productRequestDTOMock.getDescription()))
			.body("price", equalTo(productRequestDTOMock.getPrice().intValue()));
	}

	@Test
	public void alterProductWithError() {

		ProductRequestDTO productRequestDTOMock = new ProductRequestDTO("Nome", "Descrição", BigDecimal.TEN);
		
		createPut(URL_BASE + "/" + 1, productRequestDTOMock, HttpStatus.NOT_FOUND)
			.body("status_code", equalTo(HttpStatus.NOT_FOUND.value()))
			.body("message", containsString(Error.PR_1.getCode()));
	}

	@Test
	public void deleteProductWithSuccess() {

		ProductRequestDTO productRequestDTOMock = new ProductRequestDTO("Nome", "Descrição", BigDecimal.TEN);
		
		Product product = createPost(URL_BASE, productRequestDTOMock, HttpStatus.CREATED).extract().body().as(Product.class);
		
		createDelete(URL_BASE + "/" + product.getId(), HttpStatus.OK);
	}

	@Test
	public void deleteProductWithError() {

		createDelete(URL_BASE + "/" + 1, HttpStatus.NOT_FOUND)
			.body("status_code", equalTo(HttpStatus.NOT_FOUND.value()))
			.body("message", containsString(Error.PR_1.getCode()));
	}

	@Test
	public void findByIdProductWithSuccess() {

		ProductRequestDTO productRequestDTOMock = new ProductRequestDTO("Nome", "Descrição", BigDecimal.valueOf(10.0));
		
		Product product = createPost(URL_BASE, productRequestDTOMock, HttpStatus.CREATED).extract().body().as(Product.class);
		
		createGet(URL_BASE + "/" + product.getId(), HttpStatus.OK)
			.body("id", equalTo(product.getId().intValue()))
			.body("name", equalTo(productRequestDTOMock.getName()))
			.body("description", equalTo(productRequestDTOMock.getDescription()))
			.body("price", equalTo(productRequestDTOMock.getPrice().floatValue()));
	}

	@Test
	public void findByIdProductWithError() {
		createGet(URL_BASE + "/" + 1, HttpStatus.NOT_FOUND)
			.body("status_code", equalTo(HttpStatus.NOT_FOUND.value()))
			.body("message", containsString(Error.PR_1.getCode()));
	}

	@Test
	public void findAllProduct() {

		ProductRequestDTO productDTO1 = new ProductRequestDTO("Produto 1", "Descrição", BigDecimal.valueOf(10.0));
		createPost(URL_BASE, productDTO1, HttpStatus.CREATED).extract().body().as(Product.class);

		ProductRequestDTO productDTO2 = new ProductRequestDTO("Produto 2", "Descrição", BigDecimal.valueOf(10.0));
		createPost(URL_BASE, productDTO2, HttpStatus.CREATED).extract().body().as(Product.class);
	
		createGet(URL_BASE, HttpStatus.OK)
			.body("", hasSize(2));
	}
	
	@Test
	public void findAllProductWhenNoHaveProducts() {
		createGet(URL_BASE, HttpStatus.OK)
			.body("", hasSize(0));
	}

	@Test
	public void findProductsByFilterQByName() {
		
		ProductRequestDTO productDTO1 = new ProductRequestDTO("Produto 1", "Descrição", BigDecimal.valueOf(10.0));
		createPost(URL_BASE, productDTO1, HttpStatus.CREATED).extract().body().as(Product.class);

		ProductRequestDTO productDTO2 = new ProductRequestDTO("Produto 2", "Descrição", BigDecimal.valueOf(10.0));
		createPost(URL_BASE, productDTO2, HttpStatus.CREATED).extract().body().as(Product.class);
		
		RequestSpecification requestSpecification = new RequestSpecBuilder()
	            .addParam("q", "Prod")
	            .build();
		
		createGet(URL_BASE + "/search", requestSpecification, HttpStatus.OK)
			.body("", hasSize(2));
	}
	
	@Test
	public void findProductsByFilterQByDescription() {
		
		ProductRequestDTO productDTO1 = new ProductRequestDTO("Produto 1", "Descrição", BigDecimal.valueOf(10.0));
		createPost(URL_BASE, productDTO1, HttpStatus.CREATED).extract().body().as(Product.class);

		ProductRequestDTO productDTO2 = new ProductRequestDTO("Produto 2", "Descrição", BigDecimal.valueOf(10.0));
		createPost(URL_BASE, productDTO2, HttpStatus.CREATED).extract().body().as(Product.class);
		
		RequestSpecification requestSpecification = new RequestSpecBuilder()
	            .addParam("q", "Desc")
	            .build();
		
		createGet(URL_BASE + "/search", requestSpecification, HttpStatus.OK)
			.body("", hasSize(2));
	}
	
	@Test
	public void findProductsByFilterQAndMinPrice() {
		
		ProductRequestDTO productDTO1 = new ProductRequestDTO("Produto 1", "Descrição", BigDecimal.valueOf(5.0));
		createPost(URL_BASE, productDTO1, HttpStatus.CREATED).extract().body().as(Product.class);

		ProductRequestDTO productDTO2 = new ProductRequestDTO("Produto 2", "Descrição", BigDecimal.valueOf(10.0));
		createPost(URL_BASE, productDTO2, HttpStatus.CREATED).extract().body().as(Product.class);
		
		RequestSpecification requestSpecification = new RequestSpecBuilder()
	            .addParam("q", "Produto")
	            .addParam("min_price", productDTO2.getPrice())
	            .build();
		
		createGet(URL_BASE + "/search", requestSpecification, HttpStatus.OK)
			.body("", hasSize(1))
			.body("[0].name", equalTo(productDTO2.getName()));
	}
	
	@Test
	public void findProductsByFilterQAndMaxPrice() {
		
		ProductRequestDTO productDTO1 = new ProductRequestDTO("Produto 1", "Descrição", BigDecimal.valueOf(10.0));
		createPost(URL_BASE, productDTO1, HttpStatus.CREATED).extract().body().as(Product.class);

		ProductRequestDTO productDTO2 = new ProductRequestDTO("Produto 2", "Descrição", BigDecimal.valueOf(20.0));
		createPost(URL_BASE, productDTO2, HttpStatus.CREATED).extract().body().as(Product.class);
		
		RequestSpecification requestSpecification = new RequestSpecBuilder()
	            .addParam("q", "Produto")
	            .addParam("max_price", productDTO1.getPrice())
	            .build();
		
		createGet(URL_BASE + "/search", requestSpecification, HttpStatus.OK)
			.body("", hasSize(1))
			.body("[0].name", equalTo(productDTO1.getName()));
	}
	
	@Test
	public void findProductsByFilterQAndMinPriceAndMaxPrice() {
		
		ProductRequestDTO productDTO1 = new ProductRequestDTO("Produto 1", "Descrição", BigDecimal.valueOf(5.0));
		createPost(URL_BASE, productDTO1, HttpStatus.CREATED).extract().body().as(Product.class);

		ProductRequestDTO productDTO2 = new ProductRequestDTO("Produto 2", "Descrição", BigDecimal.valueOf(10.0));
		createPost(URL_BASE, productDTO2, HttpStatus.CREATED).extract().body().as(Product.class);
		
		ProductRequestDTO productDTO3 = new ProductRequestDTO("Produto 2", "Descrição", BigDecimal.valueOf(15.0));
		createPost(URL_BASE, productDTO3, HttpStatus.CREATED).extract().body().as(Product.class);
		
		RequestSpecification requestSpecification = new RequestSpecBuilder()
	            .addParam("q", "Produto")
	            .addParam("min_price", productDTO1.getPrice())
	            .addParam("max_price", productDTO2.getPrice())
	            .build();
		
		createGet(URL_BASE + "/search", requestSpecification, HttpStatus.OK)
			.body("", hasSize(2));
	}
	
	@Test
	public void findProductsByFilterMinPriceAndMaxPrice() {
		
		ProductRequestDTO productDTO1 = new ProductRequestDTO("Produto 1", "Descrição", BigDecimal.valueOf(10.0));
		createPost(URL_BASE, productDTO1, HttpStatus.CREATED).extract().body().as(Product.class);

		ProductRequestDTO productDTO2 = new ProductRequestDTO("Produto 2", "Descrição", BigDecimal.valueOf(10.0));
		createPost(URL_BASE, productDTO2, HttpStatus.CREATED).extract().body().as(Product.class);
		
		ProductRequestDTO productDTO3 = new ProductRequestDTO("Produto 2", "Descrição", BigDecimal.valueOf(15.0));
		createPost(URL_BASE, productDTO3, HttpStatus.CREATED).extract().body().as(Product.class);
		
		RequestSpecification requestSpecification = new RequestSpecBuilder()
				.addParam("min_price", productDTO1.getPrice())
	            .addParam("max_price", productDTO2.getPrice())
	            .build();
		
		createGet(URL_BASE + "/search", requestSpecification, HttpStatus.OK)
			.body("", hasSize(2));
	}
	
	@Test
	public void findProductsByFilterWhenNoHaveProducts() {
		createGet(URL_BASE + "/search", HttpStatus.OK)
			.body("", hasSize(0));
	}
	
	@Test
	public void findProductsByFilterWhenNoHaveFilters() {
		
		ProductRequestDTO productDTO1 = new ProductRequestDTO("Produto 1", "Descrição", BigDecimal.valueOf(10.0));
		createPost(URL_BASE, productDTO1, HttpStatus.CREATED).extract().body().as(Product.class);

		ProductRequestDTO productDTO2 = new ProductRequestDTO("Produto 2", "Descrição", BigDecimal.valueOf(10.0));
		createPost(URL_BASE, productDTO2, HttpStatus.CREATED).extract().body().as(Product.class);
		
		ProductRequestDTO productDTO3 = new ProductRequestDTO("Produto 2", "Descrição", BigDecimal.valueOf(15.0));
		createPost(URL_BASE, productDTO3, HttpStatus.CREATED).extract().body().as(Product.class);
		
		createGet(URL_BASE + "/search", HttpStatus.OK)
			.body("", hasSize(3));
	}
	
	private static ValidatableResponse createPost(String url, Object body, HttpStatus httpStatus) {
        return given().
                    spec(createRequestSpecification(body)).
                    log().
                        all().
               when().
                    post(url).
               then().
                    statusCode(httpStatus.value());
    }
	
	private static ValidatableResponse createPut(String url, Object body, HttpStatus httpStatus) {
        return given().
                    spec(createRequestSpecification(body)).
                    log().
                        all().
               when().
                    put(url).
               then().
                    statusCode(httpStatus.value());
    }
	
	private static ValidatableResponse createDelete(String url, HttpStatus httpStatus) {
        return given().
                    log().
                        all().
               when().
                    delete(url).
               then().
                    statusCode(httpStatus.value());
    }
	
	private static ValidatableResponse createGet(String url, HttpStatus httpStatus) {
        return given().
                    log().
                        all().
               when().
                    get(url).
               then().
                    statusCode(httpStatus.value());
    }
	
	private static ValidatableResponse createGet(String url, RequestSpecification requestSpecification, HttpStatus httpStatus) {
        return given().
        			spec(requestSpecification).
                    log().
                        all().
               when().
                    get(url).
               then().
                    statusCode(httpStatus.value());
    }
	
	private static RequestSpecification createRequestSpecification(Object body) {
        return new RequestSpecBuilder().setContentType(ContentType.JSON)
                                       .addHeader("Accept", ContentType.JSON.toString())
                                       .setBody(body)
                                       .build();
    }
}
