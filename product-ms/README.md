![Logo Compasso](https://compasso.com.br/wp-content/uploads/2020/07/LogoCompasso-Negativo.png)

# Build do Serviço

Para realizar o BUILD do projeto basta executar o comando

```javascript
  mvn clean install
```

Para executar sem rodar os testes automáticos

```javascript
  mvn clean install -DskipTests
```

# Build dos testes de integração

Para realizar o BUILD do projeto de forma que execute os testes de integração utilizamos o perfil it. Segue abaixo comando a ser executado.

```javascript
  mvn clean install -Pit
```

# Deploy da aplicação

Para realizar o deploy da aplicação basta seguir os passos abaixo:

- Acessar a pasta product-ms e executar o comando

```javascript
  mvn clean install -DskipTests
```
	
- Acessar a pasta /target e executar o comando 
	
```javascript
  java -jar products-0.0.1-SNAPSHOT.jar
```